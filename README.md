# Phone Bill

This project has the purpose of calculating the invoice of a client in his telephone calls

## Usage

```
1. Set registers of calls in resources directory
2. Call this file as phoneCalls.txt
3. Execute main
```

### How is it implemented
For this project we use memory first to improve efficiency when putting together calls to the same numbers.
We use a hashmap to have a cost of O (1) when inserting or searching for elements and finally we use a priority queue to obtain a complexity 
of O (log n) this queue has a custom ordering implemented. We will order the total cost in descending order and the numbers in ascending order