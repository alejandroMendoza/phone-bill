package phoneBill

case class Bill(totalCost: Int, phoneNumber: Int)

  extends Ordered[Bill] {

  /** Provides a comparator for priorityQueue
    *
    * @param other Object to compare
    * @return result of compare method
    */
  def compare(other: Bill) = {

    if(other.totalCost == this.totalCost) -1 * other.phoneNumber.compareTo(this.phoneNumber)
    else other.totalCost.compareTo(this.totalCost)

  }

}
