package phoneBill

import java.text.SimpleDateFormat
import java.util.Calendar

class TimeBill(input: String) {

  lazy private val number = prepareInputNumber(input)
  lazy private val time = prepareInputTime(input)
  final val PATTERN_TIME = "HH:mm:ss"

  /** This method parse the input string to obtain the number
    *
    * @param input
    * @return number
    */
  def prepareInputNumber(input: String): Int = Integer.parseInt(input.split(",")(1).replace("-", ""))

  /** This method parse the input string to obtain the resulting calendar
    *
    * @param input
    * @return Calendar
    */
  def prepareInputTime(input: String): Calendar = {

    val cleanTime = input.split (",")(0)
    val inputTimeFormat = new SimpleDateFormat(PATTERN_TIME)
    val date = inputTimeFormat.parse(cleanTime)
    val calendar = Calendar.getInstance();
    calendar.setTime(date)
    calendar

  }

  /** Returns the seconds of the call
    *
    * @return Seconds
    */
  private def getSeconds(): Int = time.get(Calendar.SECOND)

  /** Returns the minutes of the call
    *
    * @return Minutes
    */
  private def getMinutes(): Int = time.get(Calendar.MINUTE);

  /** Returns the hours of the call
    *
    * @return Hours
    */
  private def getHours(): Int = time.get(Calendar.HOUR);

  /** Returns the total seconds of the call
    *
    * @return totalSeconds
    */
  private def getTotalSeconds(): Int = getSeconds + getMinutes * 60 +  getHours * 3600;

  /** Returns the total minutes of the call
    *
    * @return totalSeconds
    */
  private def getTotalMinutes(): Int = getHours * 60 + getMinutes

  /** Returns the result bill of short call type
    *
    * @return bill
    */
  private def getShortCallPayment(): Int = getTotalSeconds * 3

  /** Returns the result bill of long call type
    *
    * @return bill
    */
  private def getLongCallPayment(): Int = {

    val startedMinute = if(getSeconds() == 0) 0 else 1
    (getTotalMinutes + startedMinute) * 150

  }

  /** Returns the number that makes the calls
    *
    * @return number
    */
  def getNumber(): Int = number

  /** Returns the duration of the call
    *
    * @return time
    */
  def getTime(): Calendar = time

  /** Returns the total cost of the call
    *
    * @return totalCost
    */
  def totalCost(): Int = {

    if(getTotalMinutes() < 5) getShortCallPayment()
    else getLongCallPayment()

  }

  /** This method is used for add time of all calls of a client
    *
    * @return added calendar
    */
  def addTime(input: Calendar)= {

    time.add(Calendar.SECOND, input.get(Calendar.SECOND))
    time.add(Calendar.MINUTE, input.get(Calendar.MINUTE))
    time.add(Calendar.HOUR, input.get(Calendar.HOUR))

  }

  override def toString: String = "Hours " + time.get(Calendar.HOUR) + " Minutes "+ time.get(Calendar.MINUTE) +
    " Seconds " + time.get(Calendar.SECOND) + " Number " + number

}
