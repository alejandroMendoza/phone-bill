package phoneBill

import scala.io.Source

object Main {

  def main(args: Array[String]): Unit = {

      val src = Source.fromFile(getClass.getClassLoader.getResource("phoneCalls.txt").getPath)
      val lines = try src.mkString finally src.close()

      println(new Calculator().execute(lines))

  }

}


