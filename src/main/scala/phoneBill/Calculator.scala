package phoneBill

import scala.collection.mutable
import org.slf4j.LoggerFactory

class Calculator {

  var priorityQueue: mutable.PriorityQueue[Bill] = mutable.PriorityQueue[Bill]().reverse
  var map = new mutable.HashMap[Int, TimeBill]
  val logger = LoggerFactory.getLogger((getClass.getSimpleName))

  /** This method calculates the invoice to be paid with the discount applied
    *
    * @param registers
    * @return total bill
    */
  def execute(registers: String): Int = {

    val splitString = registers.split("\n").filter(_.nonEmpty).toList
    var result = 0
    logger.debug("Analyzing call logs")

    if(!registers.isEmpty) {

      splitString.foreach(parseRegister(_))

      map.foreach(x => priorityQueue.enqueue(Bill(x._2.totalCost, x._1)))

      val discount = priorityQueue.dequeue
      logger.info(s"Disccount applied to number ${discount.phoneNumber} with the call cost of ${discount.totalCost}")

      priorityQueue.foreach(x => result = x.totalCost + result)

    }

    result

  }

  /** This method is responsible for obtaining all the calls with their respective durations of each phone
    *
    * @param input input lines
    */
  private def parseRegister(input: String): Unit = {

    val timeBill = new TimeBill(input)

    logger.info(s"Phone call ${timeBill}")

    if(map.contains(timeBill.getNumber)){

      val optionValue = map.get(timeBill.getNumber()).get
      timeBill.addTime(optionValue.getTime)
      map.put(timeBill.getNumber(), timeBill)

    }else{

      map.put(timeBill.getNumber(), timeBill)

    }

  }

}
