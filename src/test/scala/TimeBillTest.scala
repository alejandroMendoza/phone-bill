import java.text.ParseException

import phoneBill.TimeBill

class TimeBillTest extends BaseTestClass ("Calculator") {

  it should "return number 400234090" in {
    val timeBill = new TimeBill("00:01:07,400-234-090")
    (timeBill.getNumber should equal(400234090))
  }

  it should "return parse exception with no formatted time" in {
    val timeBill = new TimeBill("00:01A:07,400-234-090")
    val thrown = intercept[ParseException] { timeBill.totalCost }
    assert(thrown.getMessage === "Unparseable date: \"00:01A:07\"")
  }

  it should "return parse number format exception with no formatted time" in {
    val timeBill = new TimeBill("00:01A:07,40A0-234-090")
    val thrown = intercept[NumberFormatException] { timeBill.getNumber }
    assert(thrown.getMessage === "For input string: \"40A0234090\"")
  }

}