import org.scalatest.{FlatSpec, Matchers}

abstract class BaseTestClass(

  entity: String) extends FlatSpec
  with Matchers {

    behavior of entity

}
