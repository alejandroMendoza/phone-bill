import phoneBill.Calculator

class CalculatorTest extends BaseTestClass ("Calculator") {

  it should "return 0 with no calls registered" in {
    val calculator = new Calculator()
    (calculator.execute("")) should equal(0)
  }

  it should "return 0 cents as cost of calls for discount applied" in {
    val calculator = new Calculator()
    (calculator.execute("00:01:07,111-111-111")) should equal(0)
  }

  it should "calculate the total cost of calls to 201 cents" in {
    val calculator = new Calculator()
    (calculator.execute("00:01:07,111-111-111\n00:02:07,222-111-111")) should equal(201)
  }

  it should "calculate the total cost of calls to 750 cents" in {
    val calculator = new Calculator()
    (calculator.execute("00:05:00,111-111-111\n00:06:07,222-111-111")) should equal(750)
  }

  it should "calculate the total cost of calls to 900 cents" in {
    val calculator = new Calculator()
    (calculator.execute("00:01:07,400-234-090\n00:05:01,701-080-080\n00:05:00,400-234-090")) should equal(900)
  }

  it should "calculate the total cost of calls to 9150 cents" in {
    val calculator = new Calculator()
    (calculator.execute("03:01:07,400-234-090\n01:00:01,701-080-080\n00:05:00,400-234-090")) should equal(9150)
  }

}